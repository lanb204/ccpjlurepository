Nombre del proyecto: Sistema Web para El Comité de la Persona Joven del Cantón de la unión 

Integrantes: 
	1- Estaban Cruz Alfaro
	2- Justin Castillo Portilla
	3- Luis Andres Navarro Blanco
	
Descripción del proyecto:  En la actualidad las personas jóvenes del cantón no dan la importancia requerida a posibles maneras de comunicarse para realizar actividades tales como foros de opinión, por ejemplo, por lo tanto el proyecto pretende crear un sistema web llegar a las personas jóvenes desde los 13 hasta los 35 años de edad.


Cómo instalar el repositorio en el equipo para desarrollo

1- Crear una cuenta en bitbucket y unirse al repositorio de CCPJLURepository
2- Descargar e instalar sourcetree para poder clonar el repositorio a la maquina local. Y para poder hacer pull y push desde sourcetree
3- Utilizar Visual Studio 2019 para ver el proyecto y poder hacer modificaciones
