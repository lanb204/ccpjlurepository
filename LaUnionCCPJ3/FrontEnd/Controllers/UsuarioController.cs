﻿using BackEnd.DAL;
using BackEnd.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Controllers
{
    public class UsuarioController : Controller
    {

        //private  Convertir(Category category)
        //{
        //    return new CategoryViewModel
        //    {
        //        CategoryId = category.CategoryId,
        //        CategoryName = category.CategoryName,
        //        Description = category.Description
        //    };
        //}
        //     ***********VER VIDEO DONDE HABLA DEL VIEWMODEL EN FRONT END! VIDEOS CUANDO REALIZA CRUD, PROBABLENTE SEMANA DOS EN ADELANTE

        #region Lista
        public IActionResult Index()
        {

            List<Usuario> usuarios;

            using (UnidadDeTrabajo<Usuario> unidad =
                new UnidadDeTrabajo<Usuario>(new CCPJBDContext()))
            {

                usuarios = unidad.genericDAL.GetAll().ToList();
            }

            return View(usuarios);
        }
        #endregion

        #region Agregar
        public IActionResult Create()
        {
            return View();

        }

        [HttpPost]
        public IActionResult Create(Usuario usuario)
        {
            using (UnidadDeTrabajo<Usuario> Unidad
                = new UnidadDeTrabajo<Usuario>(new CCPJBDContext()))
            {
                usuario.Rol = 0;
                usuario.Contrasena = EncriptacionMD5(usuario.Contrasena);
                Unidad.genericDAL.Add(usuario);
                Unidad.Complete();
            }

            return RedirectToAction("Index");
        }
        #endregion

        #region Agregar
        public IActionResult Inicio()
        {
            return View();

        }

        [HttpPost]
        public IActionResult Inicio(Usuario usuario)
        {

            using (var contexto = new CCPJBDContext())
            {
                var resultado = (from x in contexto.Usuarios
                                 where x.CorreoUsuario == usuario.CorreoUsuario && x.Contrasena == EncriptacionMD5(usuario.Contrasena)
                                 select x).FirstOrDefault();
                if (resultado != null)
                {

                    return RedirectToAction("Index");


                }
                else
                {
                    return RedirectToAction("Inicio");
                }
            }
        }
        #endregion


        #region Eliminar

        public IActionResult Delete(int id)
        {

           return View();
        }

        [HttpPost]
        public IActionResult Delete(Usuario usuario)
        {
            using (UnidadDeTrabajo<Usuario> Unidad
            = new UnidadDeTrabajo<Usuario>(new CCPJBDContext()))
            {
                Unidad.genericDAL.Remove(usuario);
                Unidad.Complete();
            }

            return RedirectToAction("Index");
        }
        #endregion

        public static string EncriptacionMD5(string str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = md5.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");
            }
            return byte2String;
        }

    //    public ActionResult IndexPrincipal()
    //    {
    //        return View(".../Home);

    //        }

   }




}
