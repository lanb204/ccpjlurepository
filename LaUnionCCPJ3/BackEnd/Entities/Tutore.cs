﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BackEnd.Entities
{
    public partial class Tutore
    {
        public int IdTutor { get; set; }
        public string NombreTutor { get; set; }
        public string CorreoTutor { get; set; }
        public string Materia { get; set; }
        public string Ubicacion { get; set; }
        public string WebSocial { get; set; }
        public string NumeroTelefono { get; set; }
    }
}
