﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BackEnd.Entities
{
    public partial class Actividade
    {
        public int IdActividad { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
        public string Multimedia { get; set; }
        public string Distrito { get; set; }
    }
}
