﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BackEnd.Entities
{
    public partial class Foro
    {
        public int IdForos { get; set; }
        public string NombreForo { get; set; }
        public string Comentarios { get; set; }
        public string Multimedia { get; set; }
    }
}
