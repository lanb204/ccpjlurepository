﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BackEnd.Entities
{
    public partial class Galerium
    {
        public int IdGaleria { get; set; }
        public string Imagenes { get; set; }
    }
}
