﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BackEnd.Entities
{
    public partial class Comite
    {
        public int IdComite { get; set; }
        public string Depertamento { get; set; }
        public string CorreoUsuario { get; set; }
        public int Telefono { get; set; }
    }
}
