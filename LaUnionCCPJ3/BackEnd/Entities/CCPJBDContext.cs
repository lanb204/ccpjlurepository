﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace BackEnd.Entities
{
    public partial class CCPJBDContext : DbContext
    {
        public CCPJBDContext()
        {
        }

        public CCPJBDContext(DbContextOptions<CCPJBDContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Actividade> Actividades { get; set; }
        public virtual DbSet<BolsaEmpleo> BolsaEmpleos { get; set; }
        public virtual DbSet<Comite> Comites { get; set; }
        public virtual DbSet<Deporte> Deportes { get; set; }
        public virtual DbSet<Foro> Foros { get; set; }
        public virtual DbSet<Galerium> Galeria { get; set; }
        public virtual DbSet<Podcast> Podcasts { get; set; }
        public virtual DbSet<Tutore> Tutores { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=LAPTOP-P2K9RM97;Database=CCPJBD;Integrated Security=True;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Actividade>(entity =>
            {
                entity.HasKey(e => e.IdActividad);

                entity.Property(e => e.IdActividad).HasColumnName("idActividad");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Distrito)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("distrito");

                entity.Property(e => e.Fecha)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha");

                entity.Property(e => e.Multimedia)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("multimedia");
            });

            modelBuilder.Entity<BolsaEmpleo>(entity =>
            {
                entity.HasKey(e => e.IdEmpleo);

                entity.ToTable("BolsaEmpleo");

                entity.Property(e => e.IdEmpleo).HasColumnName("idEmpleo");

                entity.Property(e => e.Categoria)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("categoria");

                entity.Property(e => e.NombrePuesto)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("nombrePuesto");

                entity.Property(e => e.Salario).HasColumnName("salario");

                entity.Property(e => e.TipoTrabajo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("tipoTrabajo");

                entity.Property(e => e.Ubicacion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ubicacion");
            });

            modelBuilder.Entity<Comite>(entity =>
            {
                entity.HasKey(e => e.IdComite);

                entity.ToTable("Comite");

                entity.Property(e => e.IdComite).HasColumnName("idComite");

                entity.Property(e => e.CorreoUsuario)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("correoUsuario");

                entity.Property(e => e.Depertamento)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("depertamento");

                entity.Property(e => e.Telefono).HasColumnName("telefono");
            });

            modelBuilder.Entity<Deporte>(entity =>
            {
                entity.HasKey(e => e.IdDeporte);

                entity.Property(e => e.IdDeporte).HasColumnName("idDeporte");

                entity.Property(e => e.NombreDeporte)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("nombreDeporte");

                entity.Property(e => e.NumeroDeporte)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("numeroDeporte");

                entity.Property(e => e.Ubicacion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ubicacion");
            });

            modelBuilder.Entity<Foro>(entity =>
            {
                entity.HasKey(e => e.IdForos);

                entity.Property(e => e.IdForos).HasColumnName("idForos");

                entity.Property(e => e.Comentarios)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("comentarios");

                entity.Property(e => e.Multimedia)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("multimedia");

                entity.Property(e => e.NombreForo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("nombreForo");
            });

            modelBuilder.Entity<Galerium>(entity =>
            {
                entity.HasKey(e => e.IdGaleria);

                entity.Property(e => e.IdGaleria).HasColumnName("idGaleria");

                entity.Property(e => e.Imagenes)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("imagenes");
            });

            modelBuilder.Entity<Podcast>(entity =>
            {
                entity.HasKey(e => e.IdPodcast);

                entity.ToTable("Podcast");

                entity.Property(e => e.IdPodcast).HasColumnName("idPodcast");

                entity.Property(e => e.NombrePodcast)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("nombrePodcast");
            });

            modelBuilder.Entity<Tutore>(entity =>
            {
                entity.HasKey(e => e.IdTutor);

                entity.Property(e => e.IdTutor).HasColumnName("idTutor");

                entity.Property(e => e.CorreoTutor)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("correoTutor");

                entity.Property(e => e.Materia)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("materia");

                entity.Property(e => e.NombreTutor)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("nombreTutor");

                entity.Property(e => e.NumeroTelefono)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("numeroTelefono");

                entity.Property(e => e.Ubicacion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ubicacion");

                entity.Property(e => e.WebSocial)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("webSocial");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario);

                entity.ToTable("Usuario");

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.Property(e => e.Apellido1)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("apellido1");

                entity.Property(e => e.Apellido2)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("apellido2");

                entity.Property(e => e.Cedula)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("cedula");

                entity.Property(e => e.Contrasena)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("contrasena")
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.CorreoUsuario)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("correoUsuario");

                entity.Property(e => e.DistritoUsuario)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("distritoUsuario");

                entity.Property(e => e.NombreUsuario)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("nombreUsuario");

                entity.Property(e => e.Rol).HasColumnName("rol");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
