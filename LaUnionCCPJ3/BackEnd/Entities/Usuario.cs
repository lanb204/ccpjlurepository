﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace BackEnd.Entities
{
    public partial class Usuario
    {
        public int IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string Cedula { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string CorreoUsuario { get; set; }
        public int Rol { get; set; }
        public string DistritoUsuario { get; set; }


        [Required]
        [DataType(DataType.Password)]
        public string Contrasena { get; set; }

      
    }
}
