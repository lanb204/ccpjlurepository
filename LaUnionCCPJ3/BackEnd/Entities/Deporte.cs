﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BackEnd.Entities
{
    public partial class Deporte
    {
        public int IdDeporte { get; set; }
        public string NombreDeporte { get; set; }
        public string NumeroDeporte { get; set; }
        public string Ubicacion { get; set; }
    }
}
