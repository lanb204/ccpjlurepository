﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BackEnd.Entities
{
    public partial class Podcast
    {
        public int IdPodcast { get; set; }
        public string NombrePodcast { get; set; }
    }
}
