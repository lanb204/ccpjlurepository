﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BackEnd.Entities
{
    public partial class BolsaEmpleo
    {
        public int IdEmpleo { get; set; }
        public string NombrePuesto { get; set; }
        public string Categoria { get; set; }
        public int Salario { get; set; }
        public string Ubicacion { get; set; }
        public string TipoTrabajo { get; set; }
    }
}
